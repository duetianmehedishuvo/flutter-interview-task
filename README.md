# Flutter Interview Task

## Interview Task

Build a Dog Social Application using this [design spec](https://www.figma.com/file/bSQMB5hImn2F44fmsr9559/Flutter-Interview-Task?node-id=0%3A1).

There is no need to build any backend (api layer, server, database). For simulating form submission, you can do a POST to https://hookb.in/mZZ8pmBdk6ilzXNNzQGp

- On the Dashboard, we expect that the rows scroll horizontally

## Duration

Up to 8 hours. We do not expect you to complete the assessment in this time.

## Submission
1.  Fork this repo
2.  Build The UI using Flutter.
3.  Implement an integration test to verify that on register, the dashboard is shown
4.  Make sure to use an project architecture that clearly separates concerns
5.  Write Unit tests and Widget tests to verify that widgets work as you intend.
6.  Submit a Pull Request (PR)
7.  In the PR, include a README that includes the following:
    -  If you made any assumptions, what are they
        Ans: I don't like working with ideas. I am sure of myself first and then I do it. If there is no time, then I do not guess the work.
    - Outline what else needs to be done if given more time
        Ans: Given more time here I would do API Integration. And I would complete the rest of the screen. And in the meanwhile I used to do many kinds of queries. As if after catching all the bugs.


## How To Contact us
Name: Mehedi Hasan Shuvo
email: duarianmehedishuvo@gmail.com
whats app: +8801303129515
