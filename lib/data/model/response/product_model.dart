class ProductModel {
  int? id;
  double? rating;
  String? title;
  String? imageUrl;
  String? price;
  String? distance;

  ProductModel({this.id, this.title, this.imageUrl, this.price, this.distance,this.rating});
}
