class ChatModel {
  String? message;
  bool? isMe;
  String? messageTime;

  ChatModel({this.message, this.isMe, this.messageTime});
}