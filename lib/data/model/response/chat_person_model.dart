class ChatPersonModel {
  String? imageUrl;
  String? name;
  String? subtitle;

  ChatPersonModel({this.imageUrl, this.name, this.subtitle});
}
