import 'package:project/data/datasource/remote/dio/dio_client.dart';
import 'package:project/data/datasource/remote/exception/api_error_handler.dart';
import 'package:project/data/model/response/base/api_response.dart';

class SplashRepo {
  final DioClient? dioClient;

  SplashRepo({this.dioClient});

//stream
  Future<ApiResponse> getConfig() async {
    try {
      final response = await dioClient!.get('');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
