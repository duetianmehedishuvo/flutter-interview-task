import 'package:dio/dio.dart';
import 'package:project/data/datasource/remote/exception/api_error_handler.dart';
import 'package:project/data/model/response/base/api_response.dart';
import 'package:project/data/model/response/product_model.dart';

class ProductRepo {
  Future<ApiResponse> getProducts() async {
    try {
      List<ProductModel> _productlist = [
        ProductModel(
            id: 1,
            imageUrl:
                'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1470&q=80',
            title: 'Mason York',
            price: '\$3/h',
            distance: '7',
            rating: 4.2),
        ProductModel(
            id: 2,
            imageUrl:
                'https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1470&q=80',
            title: 'Mark Greene',
            price: '\$2/h',
            distance: '',
            rating: 5),
        ProductModel(
            id: 3,
            imageUrl:
                'https://images.unsplash.com/photo-1554151228-14d9def656e4?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=686&q=80',
            title: 'Trina Kain',
            price: '\$5/hr',
            distance: '2',
            rating: 3),
        ProductModel(
            id: 4,
            imageUrl:
                'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1470&q=80',
            title: 'Mason York',
            price: '\$3/h',
            distance: '7',
            rating: 4.2),
        ProductModel(
            id: 5,
            imageUrl:
                'https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1470&q=80',
            title: 'Mark Greene',
            price: '\$2/h',
            distance: '',
            rating: 5),
      ];
      final response = Response(data: _productlist, statusCode: 200, requestOptions: RequestOptions(path: ''));
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
