import 'package:flutter/material.dart';
import 'package:project/data/model/response/language_model.dart';
import 'package:project/util/app_constant.dart';

class LanguageRepo {
  List<LanguageModel> getAllLanguages({BuildContext? context}) {
    return AppConstant.languages;
  }
}
