import 'package:dio/dio.dart';
import 'package:project/data/datasource/remote/dio/logging_interceptor.dart';
import 'package:project/data/repository/language_repo.dart';
import 'package:project/data/repository/splash_repo.dart';
import 'package:project/provider/chat_provider.dart';
import 'package:project/provider/chat_repo.dart';
import 'package:project/provider/language_provider.dart';
import 'package:project/provider/localization_provider.dart';
import 'package:project/provider/product_provider.dart';
import 'package:project/provider/splash_provider.dart';
import 'package:project/provider/theme_provider.dart';
import 'package:project/util/app_constant.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'data/datasource/remote/dio/dio_client.dart';
import 'data/repository/product_repo.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Core
  sl.registerLazySingleton(() => DioClient(AppConstant.baseUrl, sl(), sharedPreferences: sl(), loggingInterceptor: sl()));

  // Repository
  sl.registerLazySingleton(() => LanguageRepo());
  sl.registerLazySingleton(() => ProductRepo());
  sl.registerLazySingleton(() => ChatRepo());
  sl.registerLazySingleton(() => SplashRepo(dioClient: sl()));

  // Provider
  sl.registerFactory(() => ThemeProvider(sharedPreferences: sl()));
  sl.registerFactory(() => LocalizationProvider(sharedPreferences: sl()));
  sl.registerFactory(() => LanguageProvider(languageRepo: sl()));
  sl.registerFactory(() => SplashProvider(splashRepo: sl()));
  sl.registerFactory(() => ChatProvider(chatRepo: sl()));
  sl.registerFactory(() => ProductProvider(productRepo: sl()));

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => LoggingInterceptor());
}
