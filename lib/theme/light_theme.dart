import 'package:flutter/material.dart';

ThemeData light = ThemeData(
  fontFamily: 'poppins',
  primaryColor:const Color(0xFF00594c),
  brightness: Brightness.light,
  secondaryHeaderColor: Colors.white,
  focusColor:const Color(0xFFADC4C8),
  hintColor:const Color(0xFF52575C),
);