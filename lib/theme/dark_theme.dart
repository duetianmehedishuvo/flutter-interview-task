import 'package:flutter/material.dart';

ThemeData dark = ThemeData(
    fontFamily: 'poppins',
    primaryColor: const Color(0xFF789F90),
    brightness: Brightness.dark,
    scaffoldBackgroundColor: const Color(0xFF2C2C2C),
    secondaryHeaderColor: const Color(0xFF252525),
    hintColor: const Color(0xFFE7F6F8),
    focusColor: const Color(0xFFADC4C8));
