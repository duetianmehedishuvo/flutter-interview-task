import 'package:flutter/foundation.dart';
import 'package:project/data/model/response/chat_model.dart';
import 'package:project/data/model/response/chat_person_model.dart';
import 'package:project/provider/chat_repo.dart';

class ChatProvider with ChangeNotifier {
  final ChatRepo chatRepo;

  ChatProvider({required this.chatRepo});

  // for home screen
  List<ChatPersonModel> _getAllChatModels = [];

  List<ChatPersonModel> get getAllChatModels => _getAllChatModels;

  initializeChattingData() {
    if (_getAllChatModels.isEmpty) {
      _getAllChatModels.clear();
      _getAllChatModels = chatRepo.getAllChatData;
      _getAllChatModels = chatRepo.getAllChatData;
      notifyListeners();
    }
  }

  List<ChatModel> getChatList() {
    return chatRepo.getChatList();
  }
}
