import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project/data/model/response/base/api_response.dart';
import 'package:project/data/model/response/product_model.dart';
import 'package:project/data/repository/product_repo.dart';
import 'package:project/view/widgets/custom_snackbar.dart';

class ProductProvider with ChangeNotifier {
  final ProductRepo productRepo;

  ProductProvider({required this.productRepo});

  late List<ProductModel> _productList;

  List<ProductModel> get productList => _productList;

  void getProductList(BuildContext context) async {
    ApiResponse apiResponse = await productRepo.getProducts();
    if (apiResponse.response.statusCode == 200) {
      _productList = [];
      apiResponse.response.data.forEach((category) => _productList.add(category));
      notifyListeners();
    } else {
      showCustomSnackBar(apiResponse.error.toString(), context);
    }
  }
}
