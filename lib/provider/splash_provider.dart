import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:project/data/repository/splash_repo.dart';

class SplashProvider with ChangeNotifier {
  final SplashRepo splashRepo;

  SplashProvider({required this.splashRepo});
}
