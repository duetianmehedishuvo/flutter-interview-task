
import 'package:project/data/model/response/chat_model.dart';
import 'package:project/data/model/response/chat_person_model.dart';

class ChatRepo {
  List<ChatPersonModel> getAllChatData = [
    ChatPersonModel(imageUrl: 'assets/image/person_image_one.png', name: 'Will Knowles', subtitle: 'Hey! How’s your dog? ∙ 1min'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_two.png', name: 'Ryan Bond', subtitle: 'Let’s go out! ∙ 5h'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_three.png', name: 'Sirena Paul', subtitle: 'Hey! Long time no see ∙ 1min'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_four.png', name: 'Matt Chapman', subtitle: 'You fed the dog? ∙ 6h'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_five.png', name: 'Laura Pierce', subtitle: 'How are you doing? ∙ 7h'),

    ChatPersonModel(imageUrl: 'assets/image/person_image_one.png', name: 'Will Knowles', subtitle: 'Hey! How’s your dog? ∙ 1min'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_two.png', name: 'Ryan Bond', subtitle: 'Let’s go out! ∙ 5h'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_three.png', name: 'Sirena Paul', subtitle: 'Hey! Long time no see ∙ 1min'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_four.png', name: 'Matt Chapman', subtitle: 'You fed the dog? ∙ 6h'),
    ChatPersonModel(imageUrl: 'assets/image/person_image_five.png', name: 'Laura Pierce', subtitle: 'How are you doing? ∙ 7h'),

  ];

  List<ChatModel> getChatList() {
    List<ChatModel> chatList = [
      ChatModel(message: 'Hi',isMe: false,messageTime: '11:20 AM, Yesterday'),
    ];
    return chatList;
  }
}
