class Strings {
  static const String appName = 'Flutter UI Kit Pro';
  static const String tooTiredToWalk = 'Too tired to walk your dog? Let’s help you!';
  static const String joinOurCommunity = 'Join our community';
  static const String alreadyAMember = 'Already a member? ';
  static const String signIn = 'Sign in';
  static const String signUp = 'Sign up';
  static const String letsStartHere = 'Let’s  start here';
  static const String fileInYourDetailsToBegin = 'Fill in your details to begin';
}
