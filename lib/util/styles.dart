import 'package:flutter/material.dart';
import 'package:project/util/color_resources.dart';

import 'dimensions.dart';

TextStyle poppinsLight300(BuildContext context) {
  return TextStyle(fontFamily: 'poppins', fontSize: fontSize14, fontWeight: FontWeight.w300, color: getBlackColor(context));
}

TextStyle poppinsRegular400(BuildContext context) {
  return TextStyle(fontFamily: 'poppins', fontSize: fontSize14, fontWeight: FontWeight.w400, color: getBlackColor(context));
}

TextStyle poppinsMedium500(BuildContext context) {
  return TextStyle(fontFamily: 'poppins', fontSize: 16, fontWeight: FontWeight.w500, color: getBlackColor(context));
}

TextStyle poppinsSemiBold600(BuildContext context) {
  return TextStyle(fontFamily: 'poppins', fontSize: fontSize14, fontWeight: FontWeight.w600, color: getBlackColor(context));
}

TextStyle poppinsBold700(BuildContext context) {
  return TextStyle(fontFamily: 'poppins', fontSize: 24, fontWeight: FontWeight.w700, color: getBlackColor(context));
}

TextStyle poppinsBlack900(BuildContext context) {
  return TextStyle(fontFamily: 'poppins', fontSize: fontSize14, fontWeight: FontWeight.w700, color: getBlackColor(context));
}
