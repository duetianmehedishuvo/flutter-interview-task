import 'package:flutter/material.dart';
import 'package:project/provider/theme_provider.dart';
import 'package:provider/provider.dart';

Color getPrimaryColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFFFE904B) : const Color(0xFFFE904B);
}

Color getGreyColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF404040) : const Color(0xFF797979);
}

Color getBlackColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF0D0D0D) : const Color(0xFF303030);
}

Color getRedColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFFDF2F45) : const Color(0xFFDF2F45);
}

Color getBackgroundColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF343636) : const Color(0xFFF4F7FC);
}
Color getTextPrimaryColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF343636) : const Color(0xFF63666A);
}
Color getTextSecondaryColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF343636) : const Color(0xFF63666A);
}

Color getHintColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF262626) : const Color(0xFFBDBDBD);
}

Color getSkyGreyColor(BuildContext context) {
  return Provider.of<ThemeProvider>(context).darkTheme ? const Color(0xFF121212) : const Color(0xFFDADEE3);
}

const Color primaryColor = Color(0xFFFE904B);
const Color primaryColor2 = Color(0xFFFB724C);
const appShadowColor = Color(0x95E9EBF0);
const appSecondaryBackgroundColor = Color(0xFF131d25);
const appDividerColor = Color(0xFFDADADA);
const whiteColor = Color(0xFFFCFCFC);
const darkGreyColor = Color(0xFF404040);
const greyColor = Color(0xFF7A7A7A);
const greyLightColor = Color(0xFFB0B0B0);
const backgroundColor = Color(0xFFE5E5E5);
const hintColor = Color(0xFFA1A1A1);
