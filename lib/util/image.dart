class Images {

  static String get duaIcon => '';

  //Image
  static const String placeholderImageOne = 'assets/image/placeholder_image_one.png';
  static String get splashImage => 'assets/image/onboarding.png';
  static String get logoImage => 'assets/image/LOGO.png';
  static String get onboardingImage => 'assets/image/ONBOARDING PICTURE.png';
}
