import 'package:project/data/model/response/language_model.dart';


class AppConstant {
  // API BASE URL
  static const String baseUrl = '';

  // Shared Key
  static const String theme = 'theme';
  static const String token = 'token';
  static const String countryCode = 'country_code';
  static const String languageCode = 'language_code';

  static List<LanguageModel> languages = [
    LanguageModel(imageUrl: 'Images.united_kindom', languageName: 'English', countryCode: 'US', languageCode: 'en'),
    LanguageModel(imageUrl: 'Images.arabic', languageName: 'Bangla', countryCode: 'BD', languageCode: 'bn'),
  ];
}
