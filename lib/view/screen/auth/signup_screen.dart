import 'package:flutter/material.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/strings.dart';
import 'package:project/util/styles.dart';
import 'package:project/view/screen/dashboard/dashboard_screen.dart';
import 'package:project/view/widgets/custom_button.dart';
import 'package:project/view/widgets/custom_text_field.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({Key? key}) : super(key: key);
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final FocusNode nameFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();
  final FocusNode passwordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(onTap: () {}, child: Icon(backIcon(context))),
                const SizedBox(height: 22),
                Text(Strings.letsStartHere, style: poppinsBold700(context).copyWith(fontSize: 34)),
                Text(
                  Strings.fileInYourDetailsToBegin,
                  style: poppinsMedium500(context).copyWith(fontSize: 17, color: greyColor),
                ),
                const SizedBox(height: 22),
                CustomTextField(
                  hintText: 'Fullname',
                  controller: nameController,
                  focusNode: nameFocus,
                  nextFocus: emailFocus,
                ),
                const SizedBox(height: 20),
                CustomTextField(
                  hintText: 'E-mail',
                  controller: emailController,
                  focusNode: emailFocus,
                  inputType: TextInputType.emailAddress,
                  nextFocus: passwordFocus,
                ),
                const SizedBox(height: 20),
                CustomTextField(
                  hintText: 'Password',
                  controller: passwordController,
                  focusNode: passwordFocus,
                  isPassword: true,
                  isShowSuffixIcon: true,
                  inputAction: TextInputAction.done,
                ),
                const SizedBox(height: 20),
                CustomButton(
                    btnTxt: Strings.signUp,
                    height: 55,
                    onTap: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => const DashBoardScreen()));
                    },
                    isShowRightIcon: false,
                    radius: 14),
                const SizedBox(height: 100),
                Center(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: 'By signing in, I agree with  ',
                            style: poppinsMedium500(context).copyWith(color: const Color(0xffB0B0B0), fontSize: 13)),
                        TextSpan(
                          text: 'Terms of Use\n',
                          style: poppinsMedium500(context),
                        ),
                        TextSpan(text: 'and ', style: poppinsMedium500(context).copyWith(color: const Color(0xffB0B0B0), fontSize: 13)),
                        TextSpan(
                          text: 'Privacy Policy',
                          style: poppinsMedium500(context),
                        ),
                      ],
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
