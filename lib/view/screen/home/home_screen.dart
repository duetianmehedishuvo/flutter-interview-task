import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:project/provider/product_provider.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/styles.dart';
import 'package:project/view/screen/product_details/product_details_screen.dart';
import 'package:project/view/widgets/app_widget.dart';
import 'package:project/view/widgets/custom_button.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<ProductProvider>(context, listen: false).getProductList(context);

    return Scaffold(
      body: SafeArea(
        child: ListView(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.all(20),
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Home', style: poppinsBold700(context).copyWith(fontSize: 22)),
                      Text('Explore dog walkers', style: poppinsMedium500(context).copyWith(fontSize: 16, color: greyLightColor))
                    ],
                  ),
                ),
                CustomButton(
                  btnTxt: 'Book a walk',
                  onTap: () {},
                  isShowLeftIcon: true,
                  weight: 114,
                  isShowRightIcon: false,
                  fontSize: 8,
                  height: 45,
                  radius: 12,
                ),
              ],
            ),
            const SizedBox(height: 22),
            Container(
              height: 44,
              decoration: BoxDecoration(color: const Color(0xffF0F0F0), borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Row(
                      children: [
                        const Icon(Icons.location_on_outlined, color: hintColor),
                        Text(
                          'Kiyv, Ukraine',
                          style: poppinsRegular400(context).copyWith(color: hintColor),
                        )
                      ],
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Icon(Icons.filter_list_alt, color: hintColor),
                  )
                ],
              ),
            ),
            const SizedBox(height: 23),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Near You', style: poppinsBold700(context).copyWith(fontSize: 22)),
                Text('View all', style: poppinsRegular400(context).copyWith(fontSize: 15, decoration: TextDecoration.underline)),
              ],
            ),
            const SizedBox(height: 10),
            Consumer<ProductProvider>(
                builder: (context, productProvider, child) => SizedBox(
                      height: 190,
                      child: ListView.builder(
                          itemCount: productProvider.productList.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) => productWidget(productProvider, index, context)),
                    )),
            const Divider(),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Suggested', style: poppinsBold700(context).copyWith(fontSize: 22)),
                Text('View all', style: poppinsRegular400(context).copyWith(fontSize: 15, decoration: TextDecoration.underline)),
              ],
            ),
            const SizedBox(height: 10),
            Consumer<ProductProvider>(
                builder: (context, productProvider, child) => SizedBox(
                      height: 190,
                      child: ListView.builder(
                          itemCount: productProvider.productList.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) => productWidget(productProvider, index, context)),
                    )),
          ],
        ),
      ),
    );
  }

  Widget productWidget(ProductProvider productProvider, int index, BuildContext context) {
    return InkWell(
      onTap: () {
        pushNewScreen(context, screen: ProductDetailsScreen(productModel: productProvider.productList[index]),withNavBar:false);

      },
      child: Container(
        margin: const EdgeInsets.only(right: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                networkImageWidget(productProvider.productList[index].imageUrl, height: 125, width: 180),
                Positioned(
                  top: 8,right: 17,
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 5,horizontal: 12),
                    decoration: BoxDecoration(
                      color: const Color(0xffE5E5EA).withOpacity(.2),
                      borderRadius: BorderRadius.circular(6)
                    ),
                    child: Row(children: [
                      const Icon(Icons.star_outlined,color: Color(0xffFFCB55),),
                      Text(productProvider.productList[index].rating.toString(),style: poppinsMedium500(context).copyWith(color: const Color(0xffFFCB55)),)
                    ],),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: 132,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        productProvider.productList[index].title!,
                        style: poppinsMedium500(context),
                      ),
                      Row(
                        children: [
                          const Icon(
                            Icons.location_on_outlined,
                            color: greyLightColor,
                            size: 15,
                          ),
                          Text(productProvider.productList[index].distance! + ' km from you',
                              style: poppinsMedium500(context).copyWith(fontSize: 10, color: greyLightColor)),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(15)),
                    child: Text(
                      productProvider.productList[index].price!,
                      style: poppinsMedium500(context).copyWith(color: Colors.white, fontSize: 10),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}
