import 'package:flutter/material.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/image.dart';
import 'package:project/util/strings.dart';
import 'package:project/util/styles.dart';
import 'package:project/view/screen/auth/signup_screen.dart';
import 'package:project/view/widgets/custom_button.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.all(20),
        decoration: BoxDecoration(
          image: DecorationImage(image: ExactAssetImage(Images.onboardingImage), fit: BoxFit.cover),
        ),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(Images.logoImage),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      stepWidget(context, '1', isSelected: true),
                      stepWidget(context, '2'),
                      stepWidget(context, '3', isEnd: true),
                    ],
                  ),
                  const SizedBox(height: 22),
                  Text(
                    Strings.tooTiredToWalk,
                    style: poppinsBold700(context).copyWith(fontSize: 22, color: whiteColor),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 22),
                  CustomButton(
                      btnTxt: Strings.joinOurCommunity,
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) =>  SignUpScreen()));
                      },
                      isShowRightIcon: false,
                      radius: 14),
                  const SizedBox(height: 22),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(Strings.alreadyAMember, style: poppinsRegular400(context).copyWith(color: whiteColor)),
                      Text(Strings.signIn, style: poppinsMedium500(context).copyWith(color: primaryColor)),
                    ],
                  ),
                  const SizedBox(height: 45),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 134,
                        height: 4,
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: whiteColor),
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget stepWidget(BuildContext context, String text, {bool isSelected = false, bool isEnd = false}) {
    return Row(
      children: [
        CircleAvatar(
            child: Text(text, style: poppinsRegular400(context).copyWith(color: isSelected ? Colors.black : whiteColor)),
            backgroundColor: isSelected ? whiteColor : darkGreyColor,
            radius: 20),
        isEnd
            ? const SizedBox.shrink()
            : Container(width: 10, color: const Color(0xffF7F7F8), height: 3, margin: const EdgeInsets.only(left: 10, right: 10))
      ],
    );
  }
}
