import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:project/provider/chat_provider.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/dimensions.dart';
import 'package:project/util/styles.dart';
import 'package:project/view/screen/chat/chat_details_screen.dart';
import 'package:provider/provider.dart';

class ChatScreen extends StatelessWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<ChatProvider>(context, listen: false).initializeChattingData();
    return Scaffold(
      backgroundColor: whiteColor,
      body: SafeArea(
        child: CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverPadding(
              padding: const EdgeInsets.only(left: paddingSize10, top: paddingSize10),
              sliver: SliverAppBar(
                floating: true,
                elevation: 0,
                centerTitle: false,
                automaticallyImplyLeading: false,
                //leadingWidth: 150,
                //leading: SizedBox(width: 10,),
                backgroundColor: Colors.white,
                //toolbarHeight: 30,
                title: Text('Chat', style: poppinsBold700(context).copyWith(fontSize: 22)),
              ),
            ),

            // Search Button
            SliverPersistentHeader(
                pinned: true,
                delegate: SliverDelegate(
                    child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  color: Colors.white,
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      const SizedBox(height: 10),
                      TextFormField(
                          decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(vertical: 12, horizontal: 5),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(style: BorderStyle.none, width: 0),
                              ),
                              isDense: true,
                              hintText: 'Search',
                              fillColor: const Color(0xffF0F0F0),
                              hintStyle: Theme.of(context).textTheme.headline2!.copyWith(fontSize: fontSize12, color: Colors.grey),
                              filled: true,
                              prefixIcon: const Padding(
                                padding: EdgeInsets.only(left: paddingSize20, right: paddingSize10),
                                child: Icon(Icons.search),
                              ),
                              prefixIconConstraints: const BoxConstraints(minWidth: 23, maxHeight: 25),
                              suffixIconConstraints: const BoxConstraints(minWidth: 23, maxHeight: 25),
                              suffixIcon: const Icon(Icons.filter_list_alt))),
                      const SizedBox(height: 10)
                    ],
                  ),
                ))),

            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 20, top: 20),
                child: Consumer<ChatProvider>(
                  builder: (context, chatProvider, child) => Column(
                    children: [
                      ListView.builder(
                          shrinkWrap: true,
                          reverse: true,
                          scrollDirection: Axis.vertical,
                          physics: const ClampingScrollPhysics(),
                          itemCount: chatProvider.getAllChatModels.length,
                          itemBuilder: (context, index) => Container(
                                margin: const EdgeInsets.only(bottom: 10),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 30,
                                          backgroundImage: ExactAssetImage(chatProvider.getAllChatModels[index].imageUrl!),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            pushNewScreen(context,
                                                screen: ChatDetailsScreens(chatModel: chatProvider.getAllChatModels[index]),
                                                withNavBar: false);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.only(left: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  '${chatProvider.getAllChatModels[index].name}',
                                                  style: poppinsMedium500(context).copyWith(fontSize: 20),
                                                ),
                                                const SizedBox(height: 5),
                                                Text(
                                                  '${chatProvider.getAllChatModels[index].subtitle}',
                                                  style: poppinsRegular400(context),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    const Divider()
                                  ],
                                ),
                              ))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SliverDelegate extends SliverPersistentHeaderDelegate {
  Widget child;

  SliverDelegate({required this.child});

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  double get maxExtent => 70;

  @override
  double get minExtent => 70;

  @override
  bool shouldRebuild(SliverDelegate oldDelegate) {
    return oldDelegate.maxExtent != 50 || oldDelegate.minExtent != 50 || child != oldDelegate.child;
  }
}
