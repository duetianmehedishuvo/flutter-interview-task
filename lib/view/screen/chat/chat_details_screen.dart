import 'package:flutter/material.dart';
import 'package:project/data/model/response/chat_model.dart';
import 'package:project/data/model/response/chat_person_model.dart';
import 'package:project/helper/date_converter.dart';
import 'package:project/provider/chat_repo.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/dimensions.dart';
import 'package:project/util/styles.dart';
import 'package:project/view/screen/chat/widget/message_bubble.dart';

class ChatDetailsScreens extends StatefulWidget {
  final ChatPersonModel? chatModel;

  const ChatDetailsScreens({Key? key, this.chatModel}) : super(key: key);

  @override
  _ChatDetailsScreensState createState() => _ChatDetailsScreensState();
}

class _ChatDetailsScreensState extends State<ChatDetailsScreens> {
  final TextEditingController _controller = TextEditingController();
  List<ChatModel> chatList = ChatRepo().getChatList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: [
          Expanded(
            flex: 8,
            child: Container(
              decoration: const BoxDecoration(color: whiteColor),
              child: Column(children: [
                AppBar(
                  leading: IconButton(
                      icon: const Icon(
                        Icons.arrow_back,
                        size: 28,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }),
                  backgroundColor: const Color(0xffF6F7F9),
                  centerTitle: true,
                  elevation: 0,
                  leadingWidth: 30,
                  actions: [
                    IconButton(
                        icon: const Icon(
                          Icons.call,
                          size: 28,
                          color: Colors.black,
                        ),
                        onPressed: () {})
                  ],
                  title: Row(
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: Image.asset(widget.chatModel!.imageUrl!, width: 40, height: 40, fit: BoxFit.fill)),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.chatModel!.name!, style: poppinsMedium500(context)),
                            Row(
                              children: [
                                const CircleAvatar(backgroundColor: Colors.green, radius: 5),
                                const SizedBox(width: 4),
                                Text('online', style: poppinsRegular400(context).copyWith(color: hintColor)),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    reverse: true,
                    itemCount: chatList.length,
                    itemBuilder: (context, index) {
                      List<ChatModel> reversed = chatList.reversed.toList();
                      return MessageBubble(
                        isMe: reversed[index].isMe!,
                        text: reversed[index].message!,
                        imageUrl: widget.chatModel!.imageUrl!,
                        time: reversed[index].messageTime,
                      );
                    },
                  ),
                ),
              ]),
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: whiteColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: const Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                children: [
                  IconButton(
                    icon: const Icon(
                      Icons.add,
                      color: primaryColor2,
                    ),
                    onPressed: () {},
                    padding: const EdgeInsets.all(2),
                  ),
                  Expanded(
                      child: TextFormField(
                          controller: _controller,
                          cursorColor: primaryColor2,
                          decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(vertical: 12, horizontal: 5),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: const BorderSide(style: BorderStyle.none, width: 0),
                              ),
                              isDense: true,
                              hintText: 'Start Typing',
                              fillColor: const Color(0xffF0F0F0),
                              hintStyle: Theme.of(context).textTheme.headline2!.copyWith(fontSize: fontSize12, color: Colors.grey),
                              filled: true,
                              //suffixIconConstraints: const BoxConstraints(minWidth: 23, maxHeight: 25),
                              suffixIcon: const Icon(
                                Icons.keyboard_voice,
                                size: 28,
                                color: Colors.black,
                              )))),
                  IconButton(
                    icon: const Icon(
                      Icons.send,
                      color: primaryColor2,
                    ),
                    onPressed: () {
                      chatList.add(ChatModel(message: _controller.text, isMe: true, messageTime: '${DateConverter.dateFormatStyle()} Now'));
                      _controller.text = '';
                      setState(() {});
                    },
                    padding: const EdgeInsets.all(2),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
