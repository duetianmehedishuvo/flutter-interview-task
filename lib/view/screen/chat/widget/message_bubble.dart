import 'package:flutter/material.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/styles.dart';

class MessageBubble extends StatelessWidget {
  final String text;
  final bool isMe;
  final String? time;
  final String imageUrl;

   const MessageBubble({Key? key, required this.text, required this.isMe, required this.imageUrl, this.time}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: !isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: Text(
                    time!,
                    style: poppinsMedium500(context).copyWith(fontSize: 10, color: const Color(0xffAEAEB2)),
                  ),
                ),
                Container(
                    margin: isMe
                        ? const EdgeInsets.only(top: 5, bottom: 5, left: 80, right: 10)
                        : const EdgeInsets.only(top: 5, bottom: 5, left: 10, right: 80),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(15),
                        bottomLeft: isMe ? const Radius.circular(15) : const Radius.circular(0),
                        bottomRight: isMe ? const Radius.circular(0) : const Radius.circular(15),
                        topRight: const Radius.circular(15),
                      ),
                      color: isMe ? primaryColor2 : const Color(0xffECEEF1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      child: Text(text, style: poppinsMedium500(context).copyWith(color: isMe ? whiteColor : Colors.black, fontSize: 16)),
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
