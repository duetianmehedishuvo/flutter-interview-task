import 'package:flutter/material.dart';
import 'package:project/data/model/response/product_model.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/styles.dart';
import 'package:project/view/widgets/app_widget.dart';
import 'package:project/view/widgets/custom_button.dart';

class ProductDetailsScreen extends StatelessWidget {
  final ProductModel? productModel;

  const ProductDetailsScreen({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          CustomHeader(
            productModel: productModel,
          ),
          DraggableScrollableSheet(
            initialChildSize: 0.30,
            minChildSize: 0.30,
            maxChildSize: 0.60,
            builder: (BuildContext context, ScrollController scrollController) {
              return SingleChildScrollView(
                controller: scrollController,
                physics: const BouncingScrollPhysics(),
                child: CustomScrollViewContent(productModel: productModel),
              );
            },
          ),
        ],
      ),
    );
  }
}

class CustomHeader extends StatelessWidget {
  final ProductModel? productModel;

  const CustomHeader({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        networkImageWidget(productModel!.imageUrl!, height: MediaQuery.of(context).size.height * 0.75, borderRadius: 0),
        Positioned(
            top: 40,
            left: 10,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: CircleAvatar(
                backgroundColor: const Color(0xffC4C4C4).withOpacity(.34),
                child: const Icon(Icons.close, color: whiteColor),
              ),
            )),
        Positioned(
            top: 40,
            right: 10,
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(color: const Color(0xffC4C4C4).withOpacity(.4), borderRadius: BorderRadius.circular(20)),
              child: Row(
                children: [
                  Text(
                    'Verified',
                    style: poppinsBold700(context).copyWith(color: const Color(0xffF7F7F8), fontSize: 13),
                  ),
                  const SizedBox(width: 5),
                  const Icon(Icons.check_box, color: Color(0xffF7F7F8))
                ],
              ),
            )),
      ],
    );
  }
}

class CustomScrollViewContent extends StatelessWidget {
  final ProductModel? productModel;

  const CustomScrollViewContent({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 12.0,
      shape: const RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24))),
      margin: const EdgeInsets.all(0),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: CustomInnerContent(productModel: productModel),
      ),
    );
  }
}

class CustomInnerContent extends StatelessWidget {
  final ProductModel? productModel;

  const CustomInnerContent({Key? key, this.productModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          productModel!.title!,
          style: poppinsBold700(context),
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildRow(context, "5\$", "/hr"),
            const SizedBox(width: 10),
            buildRow(context, "10", "km"),
            const SizedBox(width: 10),
            buildRow(context, "4.4", "", isShowIcon: true),
            const SizedBox(width: 10),
            buildRow(context, "450", " walks", isShowRightIcon: false),
          ],
        ),
        const SizedBox(height: 22),
        const Divider(),
        const SizedBox(height: 22),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            buildOptionButton("About", context, isCheck: true),
            buildOptionButton("Location", context),
            buildOptionButton("Reviews", context),
          ],
        ),
        const SizedBox(height: 22),
        Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Age', style: poppinsMedium500(context).copyWith(color: const Color(0xffB0B0B0), fontSize: 13)),
                Text('30 Years', style: poppinsMedium500(context).copyWith(color: const Color(0xff2B2B2B), fontSize: 17)),
              ],
            ),
            const SizedBox(width: 40),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Experience', style: poppinsMedium500(context).copyWith(color: const Color(0xffB0B0B0), fontSize: 13)),
                Text('11 months', style: poppinsMedium500(context).copyWith(color: const Color(0xff2B2B2B), fontSize: 17)),
              ],
            ),
          ],
        ),
        const SizedBox(height: 22),
        Text("""Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...""",
            style: poppinsMedium500(context).copyWith(color: const Color(0xffB0B0B0), fontSize: 13)),
        Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Read more.",
              style: poppinsMedium500(context).copyWith(color: primaryColor2, fontSize: 13),
            )),
        const SizedBox(height: 22),
        CustomButton(
          btnTxt: "Check schedule",
          onTap: () {},
          isShowRightIcon: false,
          radius: 10,
        ),
        const SizedBox(height: 22),
      ],
    );
  }

  Widget buildOptionButton(String title, BuildContext context, {bool isCheck = false}) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
      decoration: BoxDecoration(color: isCheck ? Colors.black : const Color(0xffF5F5F5), borderRadius: BorderRadius.circular(10)),
      child: Text(
        title,
        style: poppinsMedium500(context).copyWith(color: isCheck ? Colors.white : const Color(0xffB0B0B0)),
      ),
      alignment: Alignment.center,
    );
  }

  Widget buildRow(BuildContext context, String title1, String title2, {bool isShowRightIcon = true, bool isShowIcon = false}) {
    return Row(
      children: [
        Text(title1, style: poppinsMedium500(context).copyWith(fontSize: 13)),
        isShowIcon
            ? const Icon(Icons.star, size: 15, color: hintColor)
            : Text(title2, style: poppinsMedium500(context).copyWith(color: hintColor, fontSize: 13)),
        const SizedBox(width: 10),
        isShowRightIcon ? Container(height: 15, width: 1, color: hintColor) : const SizedBox.shrink(),
      ],
    );
  }
}
