import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final bool? isBackButtonExist;
  final Function? onBackPressed;
  final double? appBarSize;
  final bool? isHomeScreen;

  const CustomAppBar(
      {Key? key, required this.title, this.isBackButtonExist = true, this.onBackPressed, this.appBarSize = 70, this.isHomeScreen = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(MediaQuery.of(context).size.width, appBarSize!),
      child: SafeArea(
          child: Container(
        color: Theme.of(context).primaryColor,
        width: MediaQuery.of(context).size.width,
        child: Container(
          height: 80,
          color: Theme.of(context).scaffoldBackgroundColor,
          child: Container(
            color: Colors.white,
          ),
        ),
      )),
    );
  }

  @override
  Size get preferredSize => Size(double.maxFinite, appBarSize!);
}
