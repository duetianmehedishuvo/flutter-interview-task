import 'package:flutter/material.dart';
import 'package:project/localization/language_constrants.dart';
import 'package:project/provider/localization_provider.dart';
import 'package:project/util/color_resources.dart';
import 'package:provider/provider.dart';

class StatusWidget extends StatelessWidget {
  const StatusWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<LocalizationProvider>(
        builder: (context, localizationProvider, child) => InkWell(
              onTap: () {
                localizationProvider.setLanguage(Locale(localizationProvider.locale.countryCode == 'BD' ? "en" : 'bn',
                    localizationProvider.locale.countryCode == 'BD' ? "US" : 'BD'));
                //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SplashScreen()));
              },
              child: localizationProvider.locale.countryCode == 'BD'
                  ? Container(
                      width: 74,
                      height: 29,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: getPrimaryColor(context),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                              child: Text(
                            getTranslated('bangla', context)!,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline2!.copyWith(
                                  color: Colors.white,
                                  fontSize: 10,
                                ),
                          )),
                          const Padding(
                            padding:  EdgeInsets.all(1.0),
                            child: CircleAvatar(
                              radius: 13,
                              backgroundColor: Colors.white,
                            ),
                          )
                        ],
                      ),
                    )
                  : Container(
                      width: 74,
                      height: 29,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.grey,
                      ),
                      child: Row(
                        children: [
                          const Padding(
                            padding:  EdgeInsets.all(1.0),
                            child: CircleAvatar(
                              radius: 13,
                              backgroundColor: Colors.white,
                            ),
                          ),
                          Expanded(
                              child: Text(
                            getTranslated('english', context)!,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline2!.copyWith(
                                  color: Colors.white,
                                  fontSize: 10,
                                ),
                          )),
                        ],
                      ),
                    ),
            ));
  }
}
