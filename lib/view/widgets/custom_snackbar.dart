import 'package:flutter/material.dart';
import 'package:project/provider/theme_provider.dart';
import 'package:provider/provider.dart';

void showCustomSnackBar(String message, BuildContext context, {bool isError = true}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    backgroundColor: isError ? Colors.red : Colors.green,
    content: Text(message, style: TextStyle(color: Provider.of<ThemeProvider>(context, listen: false).darkTheme ? Colors.white : Colors.white)),
  ));
}
