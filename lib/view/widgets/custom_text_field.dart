import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project/provider/language_provider.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/util/dimensions.dart';
import 'package:project/util/styles.dart';

class CustomTextField extends StatefulWidget {
  final String? hintText;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final FocusNode? nextFocus;
  final TextInputType? inputType;
  final TextInputAction? inputAction;
  final Color? fillColor;
  final int? maxLines;
  final bool? isPassword;
  final bool? isCountryPicker;
  final bool? isShowBorder;
  final bool? isIcon;
  final bool? isShowSuffixIcon;
  final bool? isShowPrefixIcon;
  final VoidCallback? onTap;
  final VoidCallback? onChanged;
  final VoidCallback? onSuffixTap;
  final String? suffixIconUrl;
  final String? prefixIconUrl;
  final bool? isSearch;
  final VoidCallback? onSubmit;
  final bool? isEnabled;
  final TextCapitalization? capitalization;
  final LanguageProvider? languageProvider;

  const CustomTextField(
      {this.hintText = 'Write something...',
      this.controller,
      this.focusNode,
      this.nextFocus,
      this.isEnabled = true,
      this.inputType = TextInputType.text,
      this.inputAction = TextInputAction.next,
      this.maxLines = 1,
      this.onSuffixTap,
      this.fillColor,
      this.onSubmit,
      this.onChanged,
      this.capitalization = TextCapitalization.none,
      this.isCountryPicker = false,
      this.isShowBorder = false,
      this.isShowSuffixIcon = false,
      this.isShowPrefixIcon = false,
      this.onTap,
      this.isIcon = false,
      this.isPassword = false,
      this.suffixIconUrl,
      this.prefixIconUrl,
      this.isSearch = false,
      this.languageProvider,
      Key? key})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 2),
      decoration: BoxDecoration(color: widget.fillColor ?? const Color(0xffF0F0F0), borderRadius: BorderRadius.circular(10)),
      child: TextFormField(
        maxLines: widget.maxLines,
        controller: widget.controller,
        focusNode: widget.focusNode,
        style: poppinsMedium500(context).copyWith(fontSize: 17, color: const Color(0xff2B2B2B)),
        textInputAction: widget.inputAction,
        keyboardType: widget.inputType,
        cursorColor: getPrimaryColor(context),
        textCapitalization: widget.capitalization!,
        enabled: widget.isEnabled,
        autofocus: false,
        //onChanged: widget.isSearch ? widget.languageProvider.searchLanguage : null,
        obscureText: widget.isPassword! ? _obscureText : false,
        inputFormatters:
            widget.inputType == TextInputType.phone ? <TextInputFormatter>[FilteringTextInputFormatter.allow(RegExp('[0-9+]'))] : null,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 6, horizontal: 0),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(style: BorderStyle.none, width: 0),
          ),
          isDense: true,
          hintText: widget.hintText,
          labelText: widget.hintText,
          labelStyle: poppinsMedium500(context).copyWith(fontSize: 12, color: const Color(0xffAEAEB2)),
          fillColor: widget.fillColor ?? const Color(0xffF0F0F0),
          hintStyle: Theme.of(context).textTheme.headline2!.copyWith(fontSize: fontSize12, color: Colors.grey),
          filled: true,
          prefixIcon: widget.isShowPrefixIcon!
              ? Padding(
                  padding: const EdgeInsets.only(left: paddingSize20, right: paddingSize10),
                  child: Image.asset(widget.prefixIconUrl!),
                )
              : const SizedBox.shrink(),
          prefixIconConstraints: const BoxConstraints(minWidth: 23, maxHeight: 20),
          suffixIconConstraints: const BoxConstraints(minWidth: 23, maxHeight: 20),
          suffixIcon: widget.isShowSuffixIcon!
              ? widget.isPassword!
                  ? IconButton(
                      icon:
                          Icon(_obscureText ? Icons.visibility_off : Icons.visibility, color: Theme.of(context).hintColor.withOpacity(0.3)),
                      onPressed: _toggle,padding: EdgeInsets.zero,)
                  : widget.isIcon!
                      ? IconButton(
                          onPressed: widget.onSuffixTap,
                          icon: Image.asset(
                            widget.suffixIconUrl!,
                            width: 15,
                            height: 15,
                            color: Theme.of(context).textTheme.bodyText1!.color,
                          ),
                        )
                      : null
              : null,
        ),
        onTap: widget.onTap,
        // onSubmitted: (text) => widget.nextFocus != null ? FocusScope.of(context).requestFocus(widget.nextFocus) :
        // widget.onSubmit!(text),
        //onChanged: widget.onChanged!,
      ),
    );
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
}
