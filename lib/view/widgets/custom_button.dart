import 'package:flutter/material.dart';
import 'package:project/util/color_resources.dart';
import 'package:project/view/widgets/app_widget.dart';

IconData? backIcon(BuildContext context) {
  switch (Theme.of(context).platform) {
    case TargetPlatform.android:
    case TargetPlatform.fuchsia:
      return Icons.arrow_back;
    case TargetPlatform.iOS:
      return Icons.arrow_back_ios;
    case TargetPlatform.linux:
      // TODO: Handle this case.
      break;
    case TargetPlatform.macOS:
      // TODO: Handle this case.
      break;
    case TargetPlatform.windows:
      // TODO: Handle this case.
      break;
  }
  assert(false);
  return null;
}

IconData? rightIcon(BuildContext context) {
  switch (Theme.of(context).platform) {
    case TargetPlatform.android:
    case TargetPlatform.fuchsia:
      return Icons.arrow_forward;
    case TargetPlatform.iOS:
      return Icons.arrow_forward_ios;
    case TargetPlatform.linux:
      // TODO: Handle this case.
      break;
    case TargetPlatform.macOS:
      // TODO: Handle this case.
      break;
    case TargetPlatform.windows:
      // TODO: Handle this case.
      break;
  }
  assert(false);
  return null;
}

class CustomButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String? btnTxt;
  final Color? backgroundColor;
  final double? height;
  final double? weight;
  final bool? isStroked;
  final bool? isShowRightIcon;
  final bool? isShowLeftIcon;
  final double? fontSize;
  final double? radius;

  const CustomButton(
      {this.onTap,
      @required this.btnTxt,
      this.backgroundColor,
      this.height = 50.0,
      this.weight = double.infinity,
      this.fontSize = 14.0,
      this.isStroked = false,
      this.isShowRightIcon = true,
      this.isShowLeftIcon = false,
      this.radius = 4.0,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: EdgeInsets.zero,
      onPressed: onTap,
      child: Container(
        height: height,
        width: weight,
        padding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            isShowLeftIcon!
                ? Icon(Icons.add, color: isStroked! ? getPrimaryColor(context) : Colors.white,size: 16,)
                : const SizedBox.shrink(),
            Expanded(
              child: globalText(btnTxt, context,
                  textColor: isStroked! ? getPrimaryColor(context) : Colors.white,
                  isCentered: true,
                  textAllCaps: true,
                  fontSize: fontSize!),
            ),
            isShowRightIcon!
                ? Icon(rightIcon(context), color: isStroked! ? getPrimaryColor(context) : Colors.white)
                : const SizedBox.shrink()
          ],
        ),
        decoration: isStroked!
            ? boxDecoration(bgColor: Colors.transparent, borderColor: getPrimaryColor(context))
            : BoxDecoration(
                gradient: const LinearGradient(
                    colors: [primaryColor, primaryColor2],
                    begin: FractionalOffset(0.0, 0.0),
                    end: FractionalOffset(1.0, 0.0),
                    stops: [1.0, 0.0],
                    tileMode: TileMode.clamp),
                borderRadius: BorderRadius.circular(radius!)),
      ),
    );
  }
}
