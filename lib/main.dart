import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:project/provider/chat_provider.dart';
import 'package:project/provider/language_provider.dart';
import 'package:project/provider/localization_provider.dart';
import 'package:project/provider/product_provider.dart';
import 'package:project/provider/splash_provider.dart';
import 'package:project/provider/theme_provider.dart';
import 'package:project/theme/dark_theme.dart';
import 'package:project/theme/light_theme.dart';
import 'package:project/util/app_constant.dart';
import 'package:project/view/screen/onboarding/onboarding_screen.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:provider/provider.dart';
import 'di_container.dart' as di;
import 'localization/app_localization.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  await initialize();

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => di.sl<LocalizationProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<LanguageProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<SplashProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ThemeProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ProductProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ChatProvider>()),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Locale> _locals = [];
    for (var language in AppConstant.languages) {
      _locals.add(Locale(language.languageCode, language.countryCode));
    }

    return MaterialApp(
      title: 'Flutter UI Kit',
      locale: Provider.of<LocalizationProvider>(context).locale,
      theme: Provider.of<ThemeProvider>(context).darkTheme ? dark : light,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        AppLocalization.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: _locals,
      home:  const OnboardingScreen(),
    );
  }
}
